const REQUEST_TEXT = 'Футболка';

it('Correctly searches products from the top menu!', () => {
  cy.visit('/');
  cy.get('[data-test-id="input__search"]').type(REQUEST_TEXT);
  cy.get('[data-test-id="button__search"]').click();
  
  // In here, KazanExpress throws an application error, so I disabled Cypress failing because of it
  cy.on('uncaught:exception', () => false);
  cy.url().should('contain', '/search');
  
  cy.get('[data-test-id="text__title"]').contains(REQUEST_TEXT);
})
